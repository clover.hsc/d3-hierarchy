# Render the RAID Schema

### Nested RAID

![Nested Raid](src/assets/nested_raid.png)

* Data format
```javascript
{
  name: 'RAID 0',
  children: [
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
  ],
}
```

### Simple RAID
![Nested Raid](src/assets/raid.png)
```javascript
{
  name: 'RAID 5',
  children: [
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
  ],
}
```
