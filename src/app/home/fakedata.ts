export const treeData1 = {
  name: 'RAID 0',
  children: [
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
  ],
};

export const treeData2 = {
  name: 'RAID 5',
  children: [
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
    {
      name: 'Drive',
    },
  ],
};

export const treeData3 = {
  name: 'RAID 0',
  children: [
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
    {
      name: 'RAID 5',
      children: [
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
        {
          name: 'Drive',
        },
      ],
    },
  ],
};
