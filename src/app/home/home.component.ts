import { treeData1, treeData2, treeData3 } from './fakedata';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import * as d3 from 'd3';
import { HierarchyPointLink } from 'd3';
import { interval } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit {
  root: any;
  svg: any;
  g: any;
  width = 1469; // width of canvas
  height = 296; // height of canvas
  raidType = { width: 51, height: 23 }; // Blue rect of Raid type text.
  textSize = 13;
  iconSize = { width: 40, height: 40 }; // Icon size.

  NumOfNodes = 0; // Used to count how many nodes in this tree.

  constructor() {}

  ngOnInit(): void {
    this.ExeHierarchy(treeData1, 'Raid');

    // Re-render loop
    const renderTree = interval(3000);
    renderTree.subscribe((val) => {
      const i = Math.floor((Math.random() * 10) % 3);
      console.log(i);
      if (i === 0) {
        this.ExeHierarchy(treeData1, 'Raid');
      } else if (i === 1) {
        this.ExeHierarchy(treeData2, 'Raid');
      } else {
        this.ExeHierarchy(treeData3, 'Raid');
      }
    });
  }

  /**
   * Render the RAID schema.
   * 1. when nodes (include raid type) <= 48, Use normal storage icon size and dynamic change width between node & node.
   * 2. When nodes (include raid type) > 48, Use small storage icon and dynamic change width between node & node.
   * 3. When nodes (include raid type) > 92, Use small storage icon but static width between node & node and change
   *    SVG width to expose scrollbar in the tree.
   * @param treedata : Tree data.
   * @param id : Canvas id of the DOM.
   */
  ExeHierarchy(treedata: any, DomID: string): void {
    // Reset svg if exist.
    d3.select('svg').remove();

    // Initial svg attribute
    this.svg = d3.select(`div#${DomID}`).append('svg').attr('width', `100%`).attr('height', `100%`);

    // transform the posion to center in the canvas
    this.g = this.svg.append('g').attr('id', 'path');

    const sRoot = d3.hierarchy(treedata);

    // How many nodes in the tree
    this.NumOfNodes = 0;

    // Use pre-order traversal to count all nodes.
    sRoot.eachBefore((d: any) => this.NumOfNodes++);


    // Use tree() to simulate treeview layout
    let root: any;

    /* Make the tree view more friendly
      nodeSize : point out the distance between node and node.
    */
    if (this.NumOfNodes <= 92) {
      root = d3.tree().nodeSize([this.width / (this.NumOfNodes === 0 ? 10 : this.NumOfNodes), this.height / 8])(sRoot);
    } else {
      // more than 92 nodes, we use static node width size and show scroll bar in the tree section.
      root = d3.tree().nodeSize([20, this.height / 8])(sRoot);
    }

    // Get list of descendant
    const descendantsList = root.descendants();

    // Get list of leaves
    const leavesList = root.leaves();

    // Get list of none-leaves nodes.
    const noneLeavesList = descendantsList.filter((descendant: any) => {
      return !leavesList.find((leaf: any) => leaf.data.name === descendant.data.name);
    });

    // Define each line's attribute and draw the line from source to target with vector angle.
    const link = this.g
      .selectAll('.link')
      .data(root.links())
      .enter()
      .append('path')
      .attr('class', 'link')
      .attr('fill', 'none')
      .attr('stroke-width', 3)
      .attr('d', (d: HierarchyPointLink<any>) => {
        return `M${d.source.x},${d.source.y}
        L${d.target.x} ${d.source.y} L${d.target.x} ${d.target.y} `;  // vector angle
      })
      .exit()
      .remove();

    // Define each non-leaves nodes to be rectangle, border-radius and transform the position.
    const node = this.g
      .append('g')
      .attr('class', 'type-group')
      .selectAll('.node')
      .data(noneLeavesList)
      .enter()
      .append('rect')
      .attr('class', 'raid-type')
      .attr('width', this.raidType.width)
      .attr('height', this.raidType.height)
      .attr('rx', 2)
      .attr('ry', 2)
      .style('fill', '#1C7BCF')
      .attr('transform', (d: any) => `translate(${d.x - this.raidType.width / 2}, ${d.y - this.raidType.height / 2})`)
      .exit()
      .remove();

    // Define each raid type text for non-leaves node and transform position
    this.g
      .selectAll('text')
      .data(noneLeavesList)
      .enter()
      .append('text')
      .text((d: any) => d.data.name)
      .attr('class', 'mytext')
      .attr('transform', (d: any) => `translate(${d.x}, ${d.y + (this.raidType.height - this.textSize) / 2})`)
      .style('text-anchor', 'middle')
      .exit()
      .remove();

    // Define each leaves node to image and transform to correct poisition.
    const leaves = this.g
      .append('g')
      .attr('class', 'leaves-group')
      .selectAll('.leaves')
      .data(root.leaves())
      .enter()
      .append('svg:image')
      .attr('class', 'leaves')
      .attr('xlink:href', `assets/ic_LocalStorage.svg`)
      .classed('small', this.NumOfNodes > 48 ? true : false)
      .attr('transform', (d: any) => {
        if (this.NumOfNodes <= 48) {
          /* use normal icon size, so we change the icon position to
            make the line connect to center of icon
          */
          return `translate(${d.x - this.iconSize.width / 2}, ${d.y})`;
        } else {
          /* use smaller icon size, so we change the icon position to
            make the line connect to center of icon
          */
          return `translate(${d.x - this.iconSize.width / 4}, ${d.y})`;
        }
      })
      .exit()
      .remove();

    // Get the current SVG width to transform the svg coordinateto middle of tree section.
    const SVGwidth = document.querySelector('#path').getBoundingClientRect().width;

    if (SVGwidth > this.width) {
      // SVG is more bigger than tree section, so move to half distance of svg width to tree section
      this.g.attr('transform', `translate(${SVGwidth / 2}, ${this.height / 2})`);
      this.svg.attr('width', SVGwidth).attr('height', this.height);
    } else {
      // the SVG width is less than tree section, So move to half of tree width coordinate
      this.g.attr('transform', `translate(${this.width / 2}, ${this.height / 2})`);
      this.svg.attr('width', this.width).attr('height', this.height);
    }
  }
}
